define({
  "name": "Mercado Api Version 1",
  "version": "1.0.0",
  "description": "This is the documentation for the mercado api",
  "title": "Mercado Api",
  "url": "https://api.mercadolist.ng/v1",
  "sampleUrl": false,
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2016-08-19T10:02:29.207Z",
    "url": "http://apidocjs.com",
    "version": "0.16.1"
  }
});
